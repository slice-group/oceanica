require 'rubygems'
require 'sitemap_generator'

SitemapGenerator::Sitemap.default_host = 'http://oceanicalosroques.com'
SitemapGenerator::Sitemap.create do
  add '/', changefreq: 'weekly', priority: 0.9
  add '/discover', changefreq: 'weekly', priority: 0.9
  add '/diving', changefreq: 'weekly', priority: 0.9
  add '/fishing', changefreq: 'weekly', priority: 0.9
  add '/kiteboarding', changefreq: 'weekly', priority: 0.9

  group(sitemaps_path: 'en/', filename: :english) do
    add '/discover', changefreq: 'weekly', priority: 0.9
    add '/diving', changefreq: 'weekly', priority: 0.9
    add '/fishing', changefreq: 'weekly', priority: 0.9
    add '/kiteboarding', changefreq: 'weekly', priority: 0.9
  end
end
SitemapGenerator::Sitemap.ping_search_engines # Not needed if you use the rake tasks
