#Generado por keppler
require 'elasticsearch/model'
class Customer < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  has_many :reservation_customers, :dependent => :destroy
  has_many :reservation_customer_activities, :dependent => :destroy
    
  after_commit on: [:update] do
    puts __elasticsearch__.index_document
  end
  
  def self.searching(query)
    if query
      self.search(self.query query).records.order(id: :desc)
    else
      self.order(id: :desc)
    end
  end

  def self.query(query)
    { query: { multi_match:  { query: query, fields: [:name, :identification, :email] , operator: :and }  }, sort: { id: "desc" }, size: self.count }
  end

  #armar indexado de elasticserch
  def as_indexed_json(options={})
    {
      id: self.id.to_s,
      name:  self.name.to_s,
      identification:  self.identification.to_s,
      email:  self.email.to_s,
    }.as_json
  end
end
#Customer.import
