#Generado por keppler
require 'elasticsearch/model'
class Discount < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks

  validates_presence_of :code, :percentage
  validates :code,
            uniqueness: { message: 'El código ingresado ya está en uso.' }
  validates :percentage,
            uniqueness: { message: 'Este porcentaje ya posee otro código.' }

  after_commit on: [:update] do
    puts __elasticsearch__.index_document
  end

  def self.searching(query)
    if query
      self.search(self.query query).records.order(id: :desc)
    else
      self.order(id: :desc)
    end
  end

  def self.query(query)
    { query: { multi_match:  { query: query, fields: [] , operator: :and }  }, sort: { id: "desc" }, size: self.count }
  end

  #armar indexado de elasticserch
  def as_indexed_json(options={})
    {
      id: self.id.to_s,
      code:  self.code.to_s,
      percentage:  self.percentage.to_s,
    }.as_json
  end

  def full_percentage
    percentage * 100
  end
end
#Discount.import
