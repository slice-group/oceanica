#Generado por keppler
require 'elasticsearch/model'
class Offer < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  belongs_to :package
  has_many :reservations, :dependent => :destroy
  mount_uploader :image, ImageUploader

  validate :one_day_validations, on: :update

  after_commit on: [:update] do
    puts __elasticsearch__.index_document
  end

  def self.searching(query)
    if query
      self.search(self.query query).records.order(id: :desc)
    else
      self.order(id: :desc)
    end
  end

  def self.query(query)
    { query: { multi_match:  { query: query, fields: [:code, :title] , operator: :and }  }, sort: { id: "desc" }, size: self.count }
  end

  #armar indexado de elasticserch
  def as_indexed_json(options={})
    {
      id: self.id.to_s,
      code:  self.code.to_s,
      title:  self.title.to_s,
      description:   ActionView::Base.full_sanitizer.sanitize(self.description, tags: []),
      superior_price:  self.superior_price.to_s,
      standard_price:  self.standard_price.to_s,
      budget_price:  self.budget_price.to_s,
      days:  self.days.to_s,
      night:  self.night.to_s,
      package:  self.package.to_s,
    }.as_json
  end

  def one_day_validations
    errors.add(:one_day_price, 'Debe ingresar un precio.') if one_day && one_day_price.to_i.zero?
    errors.add(:one_day_description, 'Debe ingresar una pequeña descripción.') if one_day && one_day_description.eql?('')
    errors.add(:one_day_description, 'la descripción debe ser menor de 50 caracteres.') if one_day && one_day_description.length > 50
    errors.add(:one_day_description, 'la descripción debe ser mayor de 10 caracteres.') if one_day && one_day_description.length < 10
  end

  def has_prices?
    budget_price.to_i.zero? &&
    standard_price.to_i.zero? &&
    superior_price.to_i.zero? &&
    one_day_price.to_i.zero?
  end

  private
    def characters_in_description
      if self.description and self.description.size > 150
        errors.add(:description, "La descripción tiene #{self.description.size} caracteres y excede el limite de 150 caracteres.")
      end
    end

end
#Offer.import
