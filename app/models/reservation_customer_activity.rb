class ReservationCustomerActivity < ActiveRecord::Base
  belongs_to :reservation
  belongs_to :customer
  belongs_to :activity
end
