#Generado por keppler
require 'elasticsearch/model'
class Quote < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  
  after_commit on: [:update] do
    puts __elasticsearch__.index_document
  end
  
  def self.searching(query)
    if query
      self.search(self.query query).records.order(id: :desc)
    else
      self.order(id: :desc)
    end
  end

  def self.query(query)
    { query: { multi_match:  { query: query, fields: [:title, :country, :client] , operator: :and }  }, sort: { id: "desc" }, size: self.count }
  end

  #armar indexado de elasticserch
  def as_indexed_json(options={})
    {
      id: self.id.to_s,
      title:  self.title.to_s,
      quote:  self.quote.to_s,
      client:  self.client.to_s,
      country:  self.country.to_s,
    }.as_json
  end

end
#Quote.import
