#Generado por keppler
require 'elasticsearch/model'
class Reservation < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  belongs_to :offer
  has_many :reservation_customers, :dependent => :destroy
  has_many :reservation_customer_activities, :dependent => :destroy
  has_many :customers, through: :reservation_customers
  accepts_nested_attributes_for :customers, :reject_if => :all_blank, :allow_destroy => true
  before_save :verify_customers

  after_commit on: [:update] do
    __elasticsearch__.index_document
  end

  def self.searching(query)
    if query
      self.search(self.query query).records.order(id: :desc)
    else
      self.order(id: :desc)
    end
  end

  def self.query(query)
    { query: { multi_match:  { query: query, fields: [:arrival_date, :type_price, :customer_stripe, :charge_stripe] , operator: :and }  }, sort: { id: "desc" }, size: self.count }
  end

  #armar indexado de elasticserch
  def as_indexed_json(options={})
    {
      id: self.id.to_s,
      arrival_date:  self.arrival_date.to_s,
      type_price:  self.type_price.to_s,
      customer_stripe: self.customer_stripe,
      charge_stripe: self.charge_stripe,
      offer:  self.offer.to_s,
    }.as_json
  end

  def verify_customers
    self.customers.each do |customer|
      if Customer.exists? identification: customer.identification
        customer_exist = Customer.find_by_identification(customer.identification)
        customer_exist.update_attributes email: customer.email
        self.customers.delete customer
        self.reservation_customers << ReservationCustomer.new(customer_id: customer_exist.id)
      end
    end
  end

  def self.expire
  end
end
#Reservation.import
