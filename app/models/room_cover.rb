class RoomCover < ActiveRecord::Base
  belongs_to :room
  mount_uploader :url, ImageUploader
end
