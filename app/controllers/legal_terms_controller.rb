#Generado con Keppler.
class LegalTermsController < ApplicationController  
  before_filter :authenticate_user!
  layout 'admin/application'
  load_and_authorize_resource
  before_action :set_legal_term, only: [:show, :edit, :update, :destroy]

  # GET /legal_terms
  def index
    legal_terms = LegalTerm.searching(@query).all
    @objects, @total = legal_terms.page(@current_page), legal_terms.size
    redirect_to legal_terms_path(page: @current_page.to_i.pred, search: @query) if !@objects.first_page? and @objects.size.zero?
  end

  # GET /legal_terms/1
  def show
  end

  # GET /legal_terms/new
  def new
    @legal_term = LegalTerm.new
  end

  # GET /legal_terms/1/edit
  def edit
  end

  # POST /legal_terms
  def create
    @legal_term = LegalTerm.new(legal_term_params)

    if @legal_term.save
      redirect_to @legal_term, notice: 'Legal term was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /legal_terms/1
  def update
    if @legal_term.update(legal_term_params)
      redirect_to @legal_term, notice: 'Legal term was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /legal_terms/1
  def destroy
    @legal_term.destroy
    redirect_to legal_terms_url, notice: 'Legal term was successfully destroyed.'
  end

  def destroy_multiple
    LegalTerm.destroy redefine_ids(params[:multiple_ids])
    redirect_to legal_terms_path(page: @current_page, search: @query), notice: "Usuarios eliminados satisfactoriamente" 
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_legal_term
      @legal_term = LegalTerm.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def legal_term_params
      params.require(:legal_term).permit(:description)
    end
end
