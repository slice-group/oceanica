#Generado con Keppler.
class DiscountsController < ApplicationController
  before_filter :authenticate_user!
  layout 'admin/application'
  load_and_authorize_resource
  before_action :set_discount, only: [:show, :edit, :update, :destroy]
  before_action :change_percentage, only: [:create, :update]

  # GET /discounts
  def index
    discounts = Discount.searching(@query).all
    @objects, @total = discounts.page(@current_page), discounts.size
    redirect_to discounts_path(page: @current_page.to_i.pred, search: @query) if !@objects.first_page? and @objects.size.zero?
  end

  # GET /discounts/1
  def show
  end

  # GET /discounts/new
  def new
    @discount = Discount.new
  end

  # GET /discounts/1/edit
  def edit
  end

  # POST /discounts
  def create
    @discount = Discount.new(code: discount_params[:code], percentage: @percentage)

    if @discount.save
      redirect_to @discount, notice: 'Discount was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /discounts/1
  def update
    if @discount.update(code: discount_params[:code], percentage: @percentage)
      redirect_to @discount, notice: 'Discount was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /discounts/1
  def destroy
    @discount.destroy
    redirect_to discounts_url, notice: 'Discount was successfully destroyed.'
  end

  def destroy_multiple
    Discount.destroy redefine_ids(params[:multiple_ids])
    redirect_to discounts_path(page: @current_page, search: @query), notice: "Usuarios eliminados satisfactoriamente"
  end

  def by_code
    @discount = Discount.find_by code: params[:code]
    render json: @discount
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_discount
      @discount = Discount.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def discount_params
      params.require(:discount).permit(:code, :percentage)
    end

    def change_percentage
      @percentage = discount_params[:percentage].to_f
      @percentage /= 100 if @percentage > 1
    end
end
