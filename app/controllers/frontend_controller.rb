class FrontendController < ApplicationController
  layout 'layouts/frontend/application'

  def index
    @sections = [
      { selector: 'one', name: 'Discover Los Roques',
        url: discover_path },
      { selector: 'three', name: 'Scuba Diving',
        url: diving_path },
      { selector: 'four', name: 'Fishing',
        url: fishing_path },
      { selector: 'five', name: 'Kiteboarding',
        url: kiteboarding_path }
    ]
  end

  def discover
    @quotes = Quote.all
    @rooms = Room.all
    get_gallery('Discover')
    @video_code = '145182754'
  end

  def diving
    get_gallery('Scuba Diving')
    get_offers('Scuba Diving')
    @video_code = '145191498'
  end

  def fishing
    get_gallery('Fishing')
    get_offers('Fishing')
    @video_code = '145187028'
  end

  def kiteboarding
    get_gallery('Kiteboarding')
    get_offers('Kiteboarding')
    @video_code = '145188617'
  end

  def reservation
    @offer = Offer.find_by_code(params[:code])
    @activities = Activity.all
    @legal_terms = LegalTerm.first
    @message = KepplerContactUs::Message.new
  end

  private
    def get_gallery(section)
      @gallery = KepplerCatalogs::Catalog.find_by_name(section).attachments.where(public:true)
      @message = KepplerContactUs::Message.new
    end

    def get_offers(section)
      @section = section
      @offers = Package.find_by_title(section).offers
    end
end
