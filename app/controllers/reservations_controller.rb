#Generado con Keppler.
class ReservationsController < ApplicationController
  before_filter :authenticate_user!, only: [:show, :edit, :update]
  layout :layout_by_resource
  load_and_authorize_resource only: [:show, :edit, :update]
  before_action :set_reservation, only: [:show, :edit, :update, :destroy]
  skip_before_action :verify_authenticity_token
  before_action :detected_expire

  # GET /reservations
  def index
    reservations = Reservation.searching(@query).all
    @objects, @total = reservations.page(@current_page), reservations.size
    redirect_to reservations_path(page: @current_page.to_i.pred, search: @query) if !@objects.first_page? and @objects.size.zero?
  end

  # GET /reservations/1
  def show
    @discount = Discount.find_by(code: @reservation.discount_code).percentage
    @total_price = 0
    @reservation.customers.each do |customer|
      customer.reservation_customer_activities.where(reservation_id: @reservation.id).each do |activities|
        @total_price = @total_price.to_f + activities.activity.price.to_f
      end
    end
    @total_price = @total_price.to_f + (@reservation.offer["#{@reservation.type_price}_price".to_sym].to_f*@reservation.customers_number.to_f)
    @total_price = @total_price - (@total_price * @discount) if @reservation.discount_code
  end

  # GET /reservations/new
  def new
    @reservation = Reservation.new
  end

  # GET /reservations/1/edit
  def edit
  end

  # POST /reservations
  def create
    @reservation = Reservation.new(reservation_params)
    customer = Stripe::Customer.create(
      :email => params[:email],
      :card  => params[:token]
    )
    @reservation.price = params[:price]
    @reservation.email = customer.email
    @reservation.customer_stripe = customer.id
    @reservation.status = 'Pending'
    if @reservation.save
      if save_activities
        send_message 'Reserva realizada exitosamente.'
        ReservationMailer.prereserve(@reservation).deliver
        ReservationMailer.prereserve_oceanica(@reservation).deliver
      else
        send_message 'Reserva fallida.'
      end
    else
      send_message 'Reserva fallida.'
    end
  rescue Stripe::CardError => e
    flash[:error] = e.message
  end

  def create_charge
    reservation = Reservation.find(params[:reservation_id])
    amount = reservation.price.to_f * 100
    @final_amount = amount
    @code = reservation.discount_code

    unless @code.blank?
      @discount = Discount.find_by(code: @code).percentage
      unless @discount.nil?
        @discount_amount = amount * @discount
        @final_amount = amount - @discount_amount.to_i
      end

      charge_metadata = {
        :coupon_code => @code,
        :coupon_discount => (@discount * 100).to_s + "%"
      }
    end

    charge_metadata ||= {}

    charge = Stripe::Charge.create(
      :customer    => reservation.customer_stripe,
      :amount      => @final_amount.to_i,
      :description => "#{reservation.arrival_date} - #{reservation.offer.code.to_s.upcase}: #{reservation.offer.title.to_s.humanize} - #{reservation.type_price.capitalize}",
      :currency    => 'usd',
      :metadata    => charge_metadata
    )
    reservation.update charge_stripe: charge.id, status: "Approved"
    ReservationMailer.reserve(reservation).deliver
    ReservationMailer.reserve_oceanica(reservation).deliver
    redirect_to reservation_path(params[:reservation_id])
  rescue Stripe::CardError => e
    redirect_to reservation_path(params[:reservation_id])
    flash[:error] = e.message
  end

  def cancel_charge
    reservation = Reservation.find(params[:reservation_id])
    reservation.update status: "Canceled"
    ReservationMailer.reserve(reservation).deliver
    redirect_to reservation_path(params[:reservation_id])
  end

  def price_activity
    @activity = Activity.find_by_name(params[:name])
    render :json=> { :activity => @activity }, status: 200
  end

  # PATCH/PUT /reservations/1
  def update
    if @reservation.update(reservation_params)
      redirect_to @reservation, notice: 'Reservation was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /reservations/1
  def destroy
    @reservation.destroy
    redirect_to reservations_url, notice: 'Reservation was successfully destroyed.'
  end

  def destroy_multiple
    Reservation.destroy redefine_ids(params[:multiple_ids])
    redirect_to reservations_path(page: @current_page, search: @query), notice: "Usuarios eliminados satisfactoriamente"
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_reservation
      @reservation = Reservation.find(params[:id])
    end

    def save_activities
      if params[:activities]
        params[:activities].each do |activity|
          if activity[:data]
            activity[:data].each do |data|
              @activity_form = ReservationCustomerActivity.new customer_id: Customer.find_by_identification(activity[:identification]).id, activity_id: data.second[:activity_id], reservation_id: @reservation.id
              if @activity_form.save
                true
              else
                false
              end
            end
          end
        end
      end
    end

    def send_message(message)
      render :json=> { success: true, :message => message }, status: 200
    end

    def detected_expire
      Reservation.expire
    end

    # Only allow a trusted parameter "white list" through.
    def reservation_params
      params.require(:reservation).permit(:arrival_date, :type_price, :offer_id, :customers_number, :discount_code, :charge_id, customers_attributes: [:name, :identification, :email])
    end

     def layout_by_resource
      if action_name == "new" or action_name == "create"
        "frontend/application"
      else
        "admin/application"
      end
    end
end
