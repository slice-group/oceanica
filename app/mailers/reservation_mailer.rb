class ReservationMailer < ActionMailer::Base
  default from: "info@oceanicalosroques.com"

  def reserve(reservation)
  	@reservation = Reservation.find(reservation.id)
    mail(to: @reservation.email, subject: "Oceanica – Booking confirmation (#{@reservation.status})")
  end

  def reserve_oceanica(reservation)
    @reservation = Reservation.find(reservation.id)
    mail(to: "luis.prz7@gmail.com", subject: "Oceanica – Booking confirmation (#{@reservation.status})")
  end

  def prereserve(reservation)
  	@reservation = Reservation.find(reservation.id)
    mail(to: @reservation.email, subject: "Your booking with Oceanica Los Roques")
  end

  def prereserve_oceanica(reservation)
    @reservation = Reservation.find(reservation.id)
    mail(to: "luis.prz7@gmail.com", subject: "#{@reservation.email} has made a reservation and is currently in stand by for your confirmation")
  end
end