$ ->
	toggle = 0
	$('.oceanica-navbar-button').click ->
		if toggle == 0
			$('#oceanica-navbar').css 
				position: 'fixed'
			$('html, body').css	
				overflowX: 'hidden'
			$('.navbar-oceanica').css
				display: 'block'	
			$('.navbar-oceanica-link').css
				display: 'block'
			$('.navbar-oceanica').animate 
				opacity: '1'
			$('.navbar-oceanica-link').animate 
				opacity: '1',
				fontSize: '40px'
			$('main').animate 
				padding: "50px"
			toggle = 1
		else
			$('#oceanica-navbar').css 
				position: 'absolute'			
			$('html, body').css	
				overflowX: 'visible'
			$('.navbar-oceanica').animate 
				opacity: '0'
			$('.navbar-oceanica-link').animate 
				opacity: '0',	
				fontSize: '100px',
				->	
					$('.navbar-oceanica').css
						display: 'none'	
					$('.navbar-oceanica-link').css
						display: 'none'
			$('main').animate 
				padding: "0px"
			toggle = 0
