app.controller 'MainCtrl', [
	'$scope', "$compile", "$http", "$timeout", 'MainService'
	(scope, $compile, http, $timeout, MainService) ->
		# modelo para abrir y cerrar el sidebar true: open, false: close
		scope.activities = []
		scope.newActivityIndex = []
		scope.activities_content = []
		scope.reservation_errors = {}
		scope.customers_errors = []
		scope.StepOneDisabled = true
		scope.StepTwoDisabled = true
		scope.pricePackage = 0
		scope.totalPriceToPay = 0
		scope.loadingReservation = false
		scope.discount = 0
		scope.discountCode = ''
		scope.discountpercentage = 0
		scope.price = 0

		scope.participants = [
			{id: 1, name: 'Number of Participants'},
			{id: 2, name: '1 Participant'},
			{id: 3, name: '2 Participants'},
			{id: 4, name: '3 Participants'},
			{id: 5, name: '4 Participants'},
			{id: 6, name: '5 Participants'}
		];

		scope.reservationSteps = [
			{title: "Check availability"}
			{title: "Complete your booking"}
			{title: "Confirm your request"}
			{title: "Reservation complete"}
		];

		scope.reservationTitle = scope.reservationSteps[0].title

		scope.reservationDate = ->
			currentdate = new Date();
			return currentdate.getFullYear()+"-"+(currentdate.getMonth()+1)+"-"+currentdate.getDate()

		scope.initReservationModel = (type_price, offer_id, offerPrice)->
			scope.reservation = {
				type_price: type_price,
				offer_id: offer_id,
				arrival_date: ""
				customers_number: 1
			}
			scope.pricePackage = offerPrice
			return

		scope.postReservation = (token)->
			if token
				MainService.clearCustomersAttributes(scope)
				scope.reservation.customers_attributes = scope.customers_attributes
				MainService.createReservation(scope, token)
				return
			return

		scope.goToNextStep = (step)->
			stepNow = document.querySelector("#step-"+(step-1))
			stepNext = document.querySelector("#step-"+step)
			stepNowActive = document.querySelector(".step-"+(step-1))
			stepNextActive = document.querySelector(".step-"+step)
			stepNow.style.display = "none"
			stepNext.style.display = "block"
			MainService.builsCustumerForms(scope, step)
			scope.reservationTitle = scope.reservationSteps[step-1].title
			stepNowActive.id = ""
			stepNextActive.id = "active"
			return

		scope.goToBackStep = (step)->
			stepNow = document.querySelector("#step-"+(step+1))
			stepBack = document.querySelector("#step-"+step)
			stepNowActive = document.querySelector(".step-"+(step+1))
			steBackActive = document.querySelector(".step-"+step)
			stepNow.style.display = "none"
			stepBack.style.display = "block"
			scope.reservationTitle = scope.reservationSteps[step+1].title
			stepNowActive.id = ""
			steBackActive.id = "active"
			if step == 2
				console.log "step 2"
				scope.reservation.customers_number = Number(scope.reservation.customers_number) + 1
				return
			return

		scope.newActivity = (form)->
			MainService.addNewActivityInInterface(scope, form)
			return

		scope.deleteActivity = (form, activity)->
			MainService.removeActivityInInterface(scope, form, activity)
			return

		scope.deleteCustomer = (form)->
			if (Number(scope.reservation.customers_number)-1) != 1
				customer_form = document.querySelector("#customer-form-"+form)
				customer_form.style.display = "none"
				scope.customers_attributes = scope.customers_attributes.filter (customer)->
					if customer.form != form
						return customer
				scope.reservation.customers_number = Number(scope.reservation.customers_number) - 1
			return

		#Validations
		scope.validateStepOne = ->
			if scope.reservation.arrival_date == ""
				scope.reservation_errors.arrival_date = "Required Field"
			else
				delete scope.reservation_errors.arrival_date
			if (Number(scope.reservation.customers_number)-1) < 1
				scope.reservation_errors.customers_number = "Required Field"
			else
				delete scope.reservation_errors.customers_number
			if scope.reservation.discount_code != "" && scope.reservation.discount_code != undefined
				http.get(api_root+'/admin/discount/'+scope.reservation.discount_code).then (response)->
					console.log response
					if response.data == null
						scope.reservation_errors.discount_code = "The code doesn't exists"
					else
						scope.discountCode = response.data.code
						scope.discountPercentage = response.data.percentage
						delete scope.reservation_errors.discount_code
			if Object.keys(scope.reservation_errors).length == 0
				scope.goToNextStep(2)
				return
			return

		scope.validateStepTwo = ->
			validate = false
			scope.customers_attributes.filter (customer)->
				customer.errors = []
				if customer.name == undefined or customer.name == ""
					customer.errors.name = "Required Field"
					validate = true
				else
					delete customer.errors.name
					validate = false
				if customer.identification == undefined or customer.identification == ""
					customer.errors.identification = "Required Field"
					validate = true
				else
					delete customer.errors.identification
					validate = false
				if customer.email == undefined or customer.email == ""
					customer.errors.email = "Required Field"
					validate = true
				else
					delete customer.errors.email
					validate = false
			if validate == false
				scope.reservation.customers_number = Number(scope.reservation.customers_number) - 1
				MainService.buildPayReport(scope, MainService)
				scope.goToNextStep(3)
				return
			return
		return
]

