app.service 'MainService', [
	'$http'
	'$window'
	'localStorageService'
	(http, window, localStorage)->

		this.createReservation = (scope, token)->
			#arrival_date = new Date(scope.reservation.arrival_date)
			#scope.reservation.arrival_date = arrival_date.getDate()+"/"+(arrival_date.getMonth()+1)+"/"+arrival_date.getFullYear()
			stepNow = document.querySelector("#step-3")
			stepNow.style.display = "none"
			scope.loadingReservation = true
			http.post(api_root+"/reserve", {reservation: scope.reservation, activities:scope.activities, token: token.id, email: token.email, price: scope.price})
				.success(
					(data)->
						console.log data
						scope.activities_content = []
						scope.activities = {}
						scope.reservation = {}
						scope.totalPriceToPay = 0
						scope.loadingReservation = false
						scope.goToNextStep(4)
						return
				).error(
					(error)->
						console.log error
						return
				)
			return

		this.builsCustumerForms = (scope, step)->
			if step == 2
				customersNumber = Number(scope.reservation.customers_number) - 2
				scope.customers_attributes = []
				customer = 0
				while customer <= customersNumber
					scope.customers_attributes.push {form: customer}
					scope.newActivityIndex[customer] = 0
					customer++
				return
			return

		this.clearCustomersAttributes = (scope)->
			scope.customers_attributes.filter (element)->
				delete element.form
				delete element.$$hashKey
				delete element.errors
				delete element.activities
				return
			return

		this.addNewActivityInInterface = (scope, form)->
			activitySelect = document.querySelector("#activity-select-"+form)
			index = activitySelect.selectedIndex
			if activitySelect.options[index].text != "" and activitySelect.options[index].value != ""
				if scope.activities_content[form] == undefined
					scope.activities_content.push []
				if scope.activities_content[form].indexOf(activitySelect.options[index].text) == -1
					scope.activities_content[form].unshift activitySelect.options[index].text
					scope.newActivityIndex[form] = Number(scope.newActivityIndex[form]) + 1
				return
			return

		this.removeActivityInInterface = (scope, form, activity)->
			scope.activities_content[form] = scope.activities_content[form].filter (element)->
				return element != activity
			return

		this.buildPayReport = (scope, model)->
			scope.totalPriceToPay = Number(scope.pricePackage)*Number(scope.reservation.customers_number)
			scope.price = scope.totalPriceToPay
			if scope.discountCode != ''
				scope.discount = Number(scope.totalPriceToPay)*Number(scope.discountPercentage)
				scope.totalPriceToPay = Number(scope.totalPriceToPay)-Number(scope.discount)
				console.log scope.discountPercentage
				console.log scope.discount
				console.log scope.totalPriceToPay
			scope.customers_attributes.filter (customer)->
				customer.activities = []
				if scope.activities_content[customer.form] != undefined
					scope.activities_content[customer.form].filter (activity_name)->
						http.get(api_root+"/price_activity/"+activity_name.split("-")[0])
							.success(
								(data)->
									customer.activities.push data.activity
									scope.totalPriceToPay = Number(scope.totalPriceToPay)+Number(data.activity.price)
							).error(
								(error)->
									console.log error
									return
							)
			return
		return
]
