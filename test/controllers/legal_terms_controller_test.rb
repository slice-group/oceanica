require 'test_helper'

class LegalTermsControllerTest < ActionController::TestCase
  setup do
    @legal_term = legal_terms(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:legal_terms)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create legal_term" do
    assert_difference('LegalTerm.count') do
      post :create, legal_term: { description: @legal_term.description }
    end

    assert_redirected_to legal_term_path(assigns(:legal_term))
  end

  test "should show legal_term" do
    get :show, id: @legal_term
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @legal_term
    assert_response :success
  end

  test "should update legal_term" do
    patch :update, id: @legal_term, legal_term: { description: @legal_term.description }
    assert_redirected_to legal_term_path(assigns(:legal_term))
  end

  test "should destroy legal_term" do
    assert_difference('LegalTerm.count', -1) do
      delete :destroy, id: @legal_term
    end

    assert_redirected_to legal_terms_path
  end
end
