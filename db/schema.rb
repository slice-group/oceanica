# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170131204112) do

  create_table "activities", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.text     "description", limit: 65535
    t.string   "price",       limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "customers", force: :cascade do |t|
    t.string   "name",           limit: 255
    t.string   "identification", limit: 255
    t.string   "email",          limit: 255
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "discounts", force: :cascade do |t|
    t.string   "code",       limit: 255
    t.float    "percentage", limit: 24
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "keppler_catalogs_attachments", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.string   "upload",      limit: 255
    t.text     "description", limit: 65535
    t.string   "image",       limit: 255
    t.text     "url",         limit: 65535
    t.text     "target",      limit: 65535
    t.boolean  "public",      limit: 1
    t.string   "permalink",   limit: 255
    t.integer  "catalog_id",  limit: 4
    t.integer  "category_id", limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "keppler_catalogs_catalogs", force: :cascade do |t|
    t.string   "cover",       limit: 255
    t.string   "name",        limit: 255
    t.text     "description", limit: 65535
    t.string   "section",     limit: 255
    t.boolean  "public",      limit: 1
    t.string   "permalink",   limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "keppler_catalogs_categories", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "permalink",  limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "keppler_contact_us_messages", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "subject",    limit: 255
    t.string   "email",      limit: 255
    t.text     "content",    limit: 65535
    t.boolean  "read",       limit: 1
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "legal_terms", force: :cascade do |t|
    t.text     "description", limit: 65535
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "offers", force: :cascade do |t|
    t.string   "code",                limit: 255
    t.string   "title",               limit: 255
    t.string   "image",               limit: 255
    t.text     "description",         limit: 65535
    t.string   "superior_price",      limit: 255
    t.string   "standard_price",      limit: 255
    t.string   "budget_price",        limit: 255
    t.integer  "days",                limit: 4
    t.integer  "night",               limit: 4
    t.integer  "package_id",          limit: 4
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "date_begin",          limit: 255
    t.string   "date_end",            limit: 255
    t.boolean  "one_day",             limit: 1
    t.string   "one_day_price",       limit: 255
    t.string   "one_day_description", limit: 255
  end

  add_index "offers", ["package_id"], name: "index_offers_on_package_id", using: :btree

  create_table "packages", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "quotes", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.text     "quote",      limit: 65535
    t.string   "client",     limit: 255
    t.string   "country",    limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "reservation_customer_activities", force: :cascade do |t|
    t.integer  "reservation_id", limit: 4
    t.integer  "customer_id",    limit: 4
    t.integer  "activity_id",    limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "reservation_customer_activities", ["activity_id"], name: "index_reservation_customer_activities_on_activity_id", using: :btree
  add_index "reservation_customer_activities", ["customer_id"], name: "index_reservation_customer_activities_on_customer_id", using: :btree
  add_index "reservation_customer_activities", ["reservation_id"], name: "index_reservation_customer_activities_on_reservation_id", using: :btree

  create_table "reservation_customers", force: :cascade do |t|
    t.integer  "reservation_id", limit: 4
    t.integer  "customer_id",    limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "reservation_customers", ["customer_id"], name: "index_reservation_customers_on_customer_id", using: :btree
  add_index "reservation_customers", ["reservation_id"], name: "index_reservation_customers_on_reservation_id", using: :btree

  create_table "reservations", force: :cascade do |t|
    t.string   "arrival_date",     limit: 255
    t.string   "type_price",       limit: 255
    t.integer  "customers_number", limit: 4
    t.string   "email",            limit: 255
    t.string   "price",            limit: 255
    t.string   "charge_stripe",    limit: 255
    t.string   "customer_stripe",  limit: 255
    t.string   "status",           limit: 255
    t.integer  "offer_id",         limit: 4
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "discount_code",    limit: 255
  end

  add_index "reservations", ["offer_id"], name: "index_reservations_on_offer_id", using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.integer  "resource_id",   limit: 4
    t.string   "resource_type", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "room_covers", force: :cascade do |t|
    t.string   "url",        limit: 255
    t.integer  "room_id",    limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "room_covers", ["room_id"], name: "index_room_covers_on_room_id", using: :btree

  create_table "rooms", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.text     "description", limit: 65535
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "name",                   limit: 255
    t.string   "permalink",              limit: 255
    t.string   "username",               limit: 255
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id", limit: 4
    t.integer "role_id", limit: 4
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree

  add_foreign_key "offers", "packages"
  add_foreign_key "reservation_customer_activities", "activities"
  add_foreign_key "reservation_customer_activities", "customers"
  add_foreign_key "reservation_customer_activities", "reservations"
  add_foreign_key "reservation_customers", "customers"
  add_foreign_key "reservation_customers", "reservations"
  add_foreign_key "reservations", "offers"
  add_foreign_key "room_covers", "rooms"
end
