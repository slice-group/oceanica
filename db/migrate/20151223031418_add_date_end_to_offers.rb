class AddDateEndToOffers < ActiveRecord::Migration
  def change
    add_column :offers, :date_end, :string
  end
end
