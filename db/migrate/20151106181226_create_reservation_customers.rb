class CreateReservationCustomers < ActiveRecord::Migration
  def change
    create_table :reservation_customers do |t|
      t.belongs_to :reservation, index: true
      t.belongs_to :customer, index: true

      t.timestamps null: false
    end
    add_foreign_key :reservation_customers, :reservations
    add_foreign_key :reservation_customers, :customers
  end
end
