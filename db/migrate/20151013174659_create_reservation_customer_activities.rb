class CreateReservationCustomerActivities < ActiveRecord::Migration
  def change
    create_table :reservation_customer_activities do |t|
      t.belongs_to :reservation, index: true
      t.belongs_to :customer, index: true
      t.belongs_to :activity, index: true

      t.timestamps null: false
    end
    add_foreign_key :reservation_customer_activities, :reservations
    add_foreign_key :reservation_customer_activities, :customers
    add_foreign_key :reservation_customer_activities, :activities
  end
end
