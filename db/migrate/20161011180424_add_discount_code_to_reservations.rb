class AddDiscountCodeToReservations < ActiveRecord::Migration
  def change
    add_column :reservations, :discount_code, :string
  end
end
