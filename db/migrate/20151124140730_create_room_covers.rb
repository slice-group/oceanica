class CreateRoomCovers < ActiveRecord::Migration
  def change
    create_table :room_covers do |t|
      t.string :url
      t.belongs_to :room, index: true

      t.timestamps null: false
    end
    add_foreign_key :room_covers, :rooms
  end
end
