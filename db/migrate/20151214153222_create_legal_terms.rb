class CreateLegalTerms < ActiveRecord::Migration
  def change
    create_table :legal_terms do |t|
      t.text :description

      t.timestamps null: false
    end
  end
end
