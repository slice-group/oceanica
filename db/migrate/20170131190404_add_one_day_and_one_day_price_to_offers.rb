class AddOneDayAndOneDayPriceToOffers < ActiveRecord::Migration
  def change
    add_column :offers, :one_day, :boolean
    add_column :offers, :one_day_price, :string
  end
end
