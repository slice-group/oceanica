class AddOneDayDescriptionToOffers < ActiveRecord::Migration
  def change
    add_column :offers, :one_day_description, :string
  end
end
