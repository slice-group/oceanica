class CreateReservations < ActiveRecord::Migration
  def change
    create_table :reservations do |t|
      t.string :arrival_date
      t.string :type_price
      t.integer :customers_number
      t.string :email
      t.string :price
      t.string :charge_stripe
      t.string :customer_stripe
      t.string :status
      t.belongs_to :offer, index: true


      t.timestamps null: false
    end
    add_foreign_key :reservations, :offers
  end
end
