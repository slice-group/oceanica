class CreateOffers < ActiveRecord::Migration
  def change
    create_table :offers do |t|
      t.string :code
      t.string :title
      t.string :image
      t.text :description
      t.string :superior_price
      t.string :standard_price
      t.string :budget_price
      t.integer :days
      t.integer :night
      t.belongs_to :package, index: true

      t.timestamps null: false
    end
    add_foreign_key :offers, :packages
  end
end

