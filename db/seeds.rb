# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#user = CreateAdminService.new.call
#puts 'CREATED ADMIN USER: ' << user.email

[:admin].each do |name|
	Role.create name: name
	puts "#{name} creado"
end

User.create name: "Admin", email: "admin@inyxtech.com", password: "12345678", password_confirmation: "12345678", role_ids: "1"
puts "admin@inyxtech.com ha sido creado"

["Superior", "Standard", "Budget"].each do |name|
	Room.create name: name, description: ""
	puts "Habitacion/#{name} ha sido creado"
end

["Kiteboarding", "Fishing", "Scuba Diving", "Discover"].each do |name|
	KepplerCatalogs::Catalog.create name: name, description: "", section: "Galeria", public: true
	puts "Catalogo/#{name} ha sido creado"
end

LegalTerm.create description: ""

["Kiteboarding", "Fishing", "Scuba Diving"].each do |name|
	Package.create title: name
	puts "Paquete/#{name} ha sido creado"

	if name == "Kiteboarding"
		["Kiteboarding week (kiters)", "Kiteboarding week (beginners)", "Kiteboarding week in Catamaran", "One week VIP kiteboarding course with Champagne, Lobster and sea view", "", ""].each_with_index do |package, index|
			Offer.create code:"K#{index+1}", title: package, date_begin: DateTime.now.strftime("%d/%m/%Y"), date_end: (DateTime.now + 10.days).strftime("%d/%m/%Y"), superior_price: "0", standard_price: "0", budget_price: "0", days: 0, night: 0, package_id: Package.find_by_title(name).id
			puts "Paquete/#{name}/K#{index+1} creado"
		end
	end

	if name == "Fishing"
		["One week of fly fishing", "One week of fly fishing", "One week of combined fishing", "One week VIP fly fishing with Champagne, Lobster and sea view", "", ""].each_with_index do |package, index|
			Offer.create code:"F#{index+1}", title: package, date_begin: DateTime.now.strftime("%d/%m/%Y"), date_end: (DateTime.now + 10.days).strftime("%d/%m/%Y"), superior_price: "0", standard_price: "0", budget_price: "0", days: 0, night: 0, package_id: Package.find_by_title(name).id
			puts "Paquete/#{name}/F#{index+1} creado"
		end
	end

	if name == "Scuba Diving"
		["One week (for certified divers)", "One week diving course", "One week VIP diving course", "One week VIP diving course with Champagne, Lobster and sea view", "", ""].each_with_index do |package, index|
			Offer.create code:"D#{index+1}", title: package, date_begin: DateTime.now.strftime("%d/%m/%Y"), date_end: (DateTime.now + 10.days).strftime("%d/%m/%Y"), superior_price: "0", standard_price: "0", budget_price: "0", days: 0, night: 0, package_id: Package.find_by_title(name).id
			puts "Paquete/#{name}/D#{index+1} creado"
		end
	end

end

#["Kiteboarding week (kiters)", "Kiteboarding week (beginners)", "Kiteboarding week in Catamaran", "One week VIP kiteboarding course
#with Champagne, Lobster and sea view"].each_with_index do |package, index|
#	Package.create code:"K#{index+1}", title: package, superior_price: "0", standard_price: "0", budget_price: "0", days: 0, night: 0
#	puts "Paquete/K#{index+1} creado"
#end

#["One week (for certified divers)", "One week diving course", "One week VIP diving course", "One week VIP diving course with
#Champagne, Lobster and sea view",].each_with_index do |package, index|
#	Package.create code:"D#{index+1}", title: package, superior_price: "0", standard_price: "0", budget_price: "0", days: 0, night: 0
#	puts "Paquete/D#{index+1} creado"
#end